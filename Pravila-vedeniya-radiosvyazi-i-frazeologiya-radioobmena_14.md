# Какая фразеология, как правило, применяется при выполнении маршрутных полетов в контролируемом воздушном пространстве Украины, полетов на международные аэродромы Украины.
фразеология радиообмена на английском языке.

---

2.1.2. Ведення радіообміну між екіпажами повітряних суден (ПС), органами ОПР та відповідними наземними службами на території України,  у повітряному просторі України та  повітряному  просторі над  відкритим  морем,  де  відповідальність  за  ОПР покладено на Україну, здійснюється англійською або російською мовою.

З метою забезпечення радіообачливості, при виконанні маршрутних польотів у контрольованому повітряному просторі України, польотів на міжнародні аеродроми України, як правило, застосовується фразеологія радіообміну англійською мовою.