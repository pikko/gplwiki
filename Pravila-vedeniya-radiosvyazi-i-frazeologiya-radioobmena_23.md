# Когда бортовая станция может использовать сокращенный тип своего позывного
если такое сокращение выполнила первой авиационная станция.

---

3.5.6.1. Скорочення  радіотелефонного  позивного відбувається тільки після встановлення задовільного зв'язку,  а також за умови, що не виникне ніякої плутанини.

Бортова станція  може  використати   скорочений   тип   свого позивного  тільки  після того,  як таке скорочення виконала першою авіаційна станція.