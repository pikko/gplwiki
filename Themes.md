# Темы
[[ + Метеорология|Meteorologiya_0]]
[[ + Воздушное право|Vozdushnoe-pravo_0]]
[[Эксплуатация воздушных судов|Ekspluataciya-vozdushnyh-sudov_0]]
[[Летные характеристики ВС и планирование полетов|Letnye-harakteristiki-VS-i-planirovanie-poletov_0]]
[[ + Правила ведения радиосвязи и фразеология радиообмена|Pravila-vedeniya-radiosvyazi-i-frazeologiya-radioobmena_0]]
[[Эксплуатационные процедуры|Ekspluatacionnye-procedury_0]]
[[ + Воздушная навигация|Vozdushnaya-navigaciya_0]]
[[Аэродинамика полета ВС|Aerodinamika-poleta-VS_0]]
[[ + Возможности и ограничения человека в летной деятельности|Vozmozhnosti-i-ogranicheniya-cheloveka-v-letnoy-deyatelnosti_0]]

---

[Эксплуатация и техника пилотирования серийных планеров](/uploads/Themes_GPL_expluataciya_i_tehnika_pilotirovaniya_planerov.pdf)
[Шмелев Планеровождение](/uploads/Themes_GPL_Shmelev_planerovozhdenie.pdf)