# Теорема Бернулли.
для идеального несжимаемого газа уравнение Бернулли

`P + (ρV²)/2 = const`

где
`(ρV²)/2` - скоростной напор (динамическое давление), Па
`ρ` - плотность,
`V` - скорость,
`P` - статическое давление, Па.

Течение, которое удовлетворяет этому уравнению, называется несжимаемым, поскольку оно применимо как к жидкостям, которые практически несжимаемы, так и к газам, если скорости их движения малы по сравнению со скоростью звука. Если скорость в какой-либо точке потока больше половины скорости звука, то расчеты по этой формуле будут содержать значительные погрешности. Такие течения называются сжимаемыми.

Уравнение Бернулли можно пояснить на примере суживающегося трубопровода, через который течёт газ

![](/uploads/Aerodinamika-poleta-VS_2_bernulli.jpg)

В узком сечении трубы скорость больше, чем в широком, поэтому в сужающейся трубе газ движется ускоренно. Ускоренное движение возможно только под действием силы. Эта сила создаётся разностью давлений. Следовательно, давление в широкой части трубы должно быть больше, чем в узкой.